# MAC0110 - MiniEP7
# Luã Nowacki Scavacini Santilli - 11795492

using Test

function bernoulli(n)
    n *= 2
    A = Vector{Rational{BigInt}}(undef, n + 1)
    for m = 0 : n
        A[m + 1] = 1 // (m + 1)
        for j = m : -1 : 1
            A[j] = j * (A[j] - A[j + 1])
        end
    end
    return abs(A[1])
end


function taylor_sin(x)
    V = Array{BigFloat, 1}(undef, 10)
    F = Array{BigInt, 1}(undef, 19)
    F[1] = 1
    for i in 2:19
        F[i] = F[i-1] * i
    end
    V[1] = x
    xtwo = x^2
    for i in 2:10
        V[i] = V[i-1] * xtwo * -1
    end
    for i in 2:10
        V[i] /= F[2*i - 1]
    end
    total = 0
    for el in V
        total += el
    end
    return total
end

function taylor_cos(x)
    V = Array{BigFloat, 1}(undef, 10)
    F = Array{BigInt, 1}(undef, 19)
    F[1] = 1
    for i in 2:19
        F[i] = F[i-1] * i
    end
    V[1] = 1
    xtwo = x^2
    for i in 2:10
        V[i] = V[i-1] * xtwo * -1
    end
    for i in 2:10
        V[i] /= F[2*(i-1)] 
    end
    total = 0
    for el in V
        total += el
    end
    return total
end

function taylor_tan(x) 
    V = Array{BigFloat, 1}(undef, 10)
    F = Array{BigInt, 1}(undef, 20)
    F[1] = 1
    for i in 2:20
        F[i] = F[i-1] * i
    end
    V[1] = x
    for i in 2:10
        V[i] = (2^(2*i) * (2^(2*i) - 1) * bernoulli(i) * (x^(2*i - 1)))
        V[i] /= F[2*i]
    end
    total = 0
    for el in V
        total += el
    end
    return total
end

function almost_equal(v1, v2)
    if abs(v2 - v1) < 0.000001
        return true
    else
        return false
    end
end

function check_sin(value, x)
    if almost_equal(value, sin(x))
        return true
    else 
        return false
    end
end

function check_cos(value, x)
    if almost_equal(value, cos(x))
        return true
    else
        return false
    end
end

function check_tan(value, x)
    if almost_equal(value, tan(x))
        return true
    else 
        return false
    end
end

function test()
    @test check_sin(taylor_sin(0), 0)
    @test check_sin(taylor_sin(pi / 4), (pi / 4))
    @test check_sin(taylor_sin(pi / 2), (pi / 2))
    @test check_sin(taylor_sin(pi / 3), (pi / 3))
    @test check_cos(taylor_cos(0), 0)
    @test check_cos(taylor_cos(pi / 4), (pi / 4))
    @test check_cos(taylor_cos(pi / 2), (pi / 2))
    @test check_cos(taylor_cos(pi / 3), (pi / 3))
    @test check_tan(taylor_tan(0), 0)
    @test check_tan(taylor_tan(pi / 4), (pi / 4))
    @test check_tan(taylor_tan(pi / 3), (pi / 3))
    println("The functions sin, cos, and tan are working")
end

test()
